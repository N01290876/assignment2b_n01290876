﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Application2b._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Reina's Study Guide</h1>
        <p class="lead">This website is a study guide for the courses <strong>Database Design</strong>, <strong>Web Programming</strong> and <strong>Digital Design</strong>. </p>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h3>About me:</h3>
           <p>Hi. I'm Reina and i'm currently studying Web Development at Humber College. This website contains some interesting facts on the courses which i am doing in the first semester.</p>
        </div>
        <div class="col-md-6">
            <h3>Below are the points catered in this study guide:</h3>
              <ul>
                  <li>A tricky concept I have learnt in each course.</li>
                  <li>An example of a snippet of code that I wrote myself, with a caption describing what it does.</li>
                  <li>An example of a snippet of code that I found interesting and I want to share it with you all.</li>
                  <li>Helpful links.</li>
              </ul>
        </div>
  
    </div>

</asp:Content>
