﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Application2b.usercontrol
{
    public partial class Database : System.Web.UI.UserControl
    {
        DataView CreateCodeSource()
        {
            DataTable codedata3 = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col3 = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col3 = new DataColumn();

            DataRow coderow3;

            idx_col3.ColumnName = "Line";
            idx_col3.DataType = System.Type.GetType("System.Int32");

            codedata3.Columns.Add(idx_col3);

            code_col3.ColumnName = "Code";
            code_col3.DataType = System.Type.GetType("System.String");
            codedata3.Columns.Add(code_col3);

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>
            List<string> front_end_code = new List<string>(new string[]{

                "~ SELECT sysdate FROM DUAL"
        
            });

            int i = 0;
            foreach (string code_line in front_end_code)
            {
                coderow3 = codedata3.NewRow();
                coderow3[idx_col3.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow3[code_col3.ColumnName] = formatted_code;

                i++;
                codedata3.Rows.Add(coderow3);
            }


            DataView codeview3 = new DataView(codedata3);
            return codeview3;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView db = CreateCodeSource();
            Database_snippet.DataSource = db;

            /*Some formatting in the codebehind*/


            Database_snippet.DataBind();
        }
    }
}