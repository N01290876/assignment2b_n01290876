﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Application2b.usercontrol
{
    public partial class WebProgramming : System.Web.UI.UserControl
    {
        DataView CreateCodeSource()
        {
            DataTable codedata1 = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col1 = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col1 = new DataColumn();

            DataRow coderow1;

            idx_col1.ColumnName = "Line";
            idx_col1.DataType = System.Type.GetType("System.Int32");

            codedata1.Columns.Add(idx_col1);

            code_col1.ColumnName = "Code";
            code_col1.DataType = System.Type.GetType("System.String");
            codedata1.Columns.Add(code_col1);

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>

            
               List<string> front_end_code = new List<string>(new string[]{
               
            "~var fruits = [Kiwi, Banana, Apple];",
            "~fruits.push(Orange);",

            });

            int i = 0;
            foreach (string code_line in front_end_code)
            {
                coderow1 = codedata1.NewRow();
                coderow1[idx_col1.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow1[code_col1.ColumnName] = formatted_code;

                i++;
                codedata1.Rows.Add(coderow1);
            }


            DataView codeview1 = new DataView(codedata1);
            return codeview1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView wp = CreateCodeSource();
            WProgram_snippet.DataSource = wp;

            /*Some formatting in the codebehind*/


            WProgram_snippet.DataBind();

        }
    }
}