﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Application2b.usercontrol
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        DataView CreateCodeSource()
        {
            DataTable codedata2 = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col2 = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col2 = new DataColumn();

            DataRow coderow2;

            idx_col2.ColumnName = "Line";
            idx_col2.DataType = System.Type.GetType("System.Int32");

            codedata2.Columns.Add(idx_col2);

            code_col2.ColumnName = "Code";
            code_col2.DataType = System.Type.GetType("System.String");
            codedata2.Columns.Add(code_col2);

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>
            List<string> front_end_code = new List<string>(new string[]{


              
                "~<ul>",
                "~~~~<li>Coffee</li>",
                "~~~~<li>Tea",
                "~~~~~~~<ul>",
                "~~~~~~~~<li>White tea</li>",
                "~~~~~~~</ul>",
                "~~~~</li>",
                "~~~~<li>Milk</li>",
                "~</ul>"
            });

            int i = 0;
            foreach (string code_line in front_end_code)
            {
                coderow2 = codedata2.NewRow();
                coderow2[idx_col2.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow2[code_col2.ColumnName] = formatted_code;

                i++;
                codedata2.Rows.Add(coderow2);
            }


            DataView codeview2 = new DataView(codedata2);
            return codeview2;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Digital_snippet.DataSource = ds;

            /*Some formatting in the codebehind*/


            Digital_snippet.DataBind();

        }
    }
}